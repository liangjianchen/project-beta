# CarCar

CarCar is an application using Django, React, and JSON for managing the aspects of an automobile dealership. It handles inventory management, automobile sales, and automobile services.

Team:
Liangjian Chen - inventory , servives full-stack
               - sales back-end API(Not complete)
## Getting Started

**Make sure you have Docker, Git, and Node.js 18.2 or above**
1. Fork this repository

2. Clone the forked repository onto your local computer:
git clone <<respository.url.here>>

3. Build and run the project using Docker with these commands:
```
docker volume create beta-data
docker-compose build
docker-compose up
```

- View the main page in the browser: http://localhost:3000/

## Design
CarCar consists of three microservices, namely Inventory, Services, and Sales, which interact with each other

- **Inventory**
- **Services**
- **Sales**

The process begins at our inventory domain where we maintain a record of automobiles available for purchase on our lot. To ensure that our service and sales teams always have up-to-date information, our sales and service microservices utilize a poller. This poller communicates with the inventory domain, keeping track of the vehicles in our inventory.

## Accessing Endpoints to Send and View Data: Access Through Insomnia & Your Browser

### Manufacturers:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create a manufacturer | POST | http://localhost:8100/api/manufacturers/ |
| Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/id/
| Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/id/
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/id/

To create Manufacturers
```
{
  "name": "Chrysler"
}

return
```
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Chrysler"
}

### Vehicle Models:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List vehicle models | GET | http://localhost:8100/api/models/
| Create a vehicle model | POST | http://localhost:8100/api/models/
| Get a specific vehicle model | GET | http://localhost:8100/api/models/id/
| Update a specific vehicle model | PUT | http://localhost:8100/api/models/id/
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/id/

To create Vehicle Models
```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}

return
```
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Chrysler"
	}
}

### Automobiles:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List automobiles | GET | http://localhost:8100/api/automobiles/
| Create an automobile | POST | http://localhost:8100/api/automobiles/
| Get a specific automobile | GET | http://localhost:8100/api/automobiles/vin/
| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/vin/
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/vin/


To create Automobiles
```
{
  "color": "gold",
  "year": 2012,
  "vin": "2HKRM4H35GH688819",
  "model_id": 1
}

return
```
{
	"href": "/api/automobiles/2HKRM4H35GH688819/",
	"id": 1,
	"color": "gold",
	"year": 2012,
	"vin": "2HKRM4H35GH688819",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	},
	"sold": false
}
## Service microservice

The AutoMobile Service provides functionality to list and create technicians. Each technician has properties including their name and employee ID.

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List technicians | GET | http://localhost:8080/api/technicians/
| Technician detail | GET | http://localhost:8080/api/technicians/<int:id>/
| Create a technician | POST | http://localhost:8080/api/technicians/
| Delete a technician | DELETE | http://localhost:8080/api/technicians/<int:id>/

To create technicians
```
{
  "first_name": "David",
  "last_name": "Fanrk",
	"employee_id":3
}
return
```
{
	"href": "/api/technicians/4",
	"first_name": "David",
	"last_name": "Fanrk",
	"employee_id": 3,
	"id": 4
}

The AutoMobile Service incorporates service appointments, which include the following details: the VIN of the vehicle, the customer's name, the date and time of the appointment, the assigned technician, and the reason for the service appointment. Additionally, there are two special features:
1. VIP: This feature offers exclusive benefits to VIP customers.
2. Appointment Status: This feature tracks and displays the status of each appointment.
The service appointment list showcases scheduled appointments, providing the collected information in the following format: VIN, customer name, date and time of the appointment, the assigned technician's name, and the reason for the service.

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List service appointments | GET | http://localhost:8080/api/appointments/
| Service appointment detail | GET | http://localhost:8080/api/appointments/<int:id>
| Create service appointment | POST | http://localhost:8080/api/appointments/
| Delete service appointment | DELETE | http://localhost:8080/api/appointments/<int:id>

To create service appointments
```
{
  "vin": "1C3CC5FB2AN120174",
  "customer": "Fanrk",
	"date_time":"2023-06-07T23:44:00+00:00",
	"reason":"oil chage",
	"technician": "1"
}
return
```
{
	"href": "/api/appointments/9",
	"vin": "1C3CC5FB2AN120174",
	"date_time": "2023-06-07T23:44:00+00:00",
	"status": false,
	"reason": "oil chage",
	"customer": "Fanrk",
	"technician": {
		"href": "/api/technicians/1",
		"first_name": "Bin",
		"last_name": "James",
		"employee_id": 1,
		"id": 1
	},
	"vip": true,
	"id": 9,
	"operate": "Create"
}

The service history feature displays a comprehensive record of all service appointments, including both current and canceled or finished appointments.

| Action | Method | URL
| ----------- | ----------- | ----------- |
| Service appointment history | GET | http://localhost:8080/api/search/<vin>


## Sales microservice

The Sales functionality is designed to maintain accurate records of automobile sales that originate from the inventory. There are two essential rules enforced:

1. A person cannot sell a car that is not listed in the inventory: The system ensures that only vehicles present in the inventory can be sold. This prevents the sale of cars that are not officially available for purchase.

2. A person cannot sell a car that has already been sold: The system tracks the sales transactions and prohibits the sale of vehicles that have already been sold. This avoids any duplication or inconsistencies in the sales records.


salespeople

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List salespeople | GET | http://localhost:8090/api/salespeople/
| Create a salesperson | POST | http://localhost:8090/api/salespeople/
| Delete a specific salesperson | DELETE | http://localhost:8090/api/salespeople/<int:id>

To create salespeople
```
{
  "first_name": "James",
  "last_name": "Fanrk",
	"employee_id":2
}


customers

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List customers | GET | http://localhost:8090/api/customers/
| Create a customers | POST | http://localhost:8090/api/customers/
| Delete a specific customers | DELETE | http://localhost:8090/api/customers/<int:id>

To create customers
```
{
  "first_name": "kkames11",
  "last_name": "Ganrk11",
	"address":"3990 E exaple",
	"phone_number":"0005553333"
}

sales

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List sales | GET | http://localhost:8090/api/sales/
| Create a sales | POST | http://localhost:8090/api/sales/
| Delete a specific sales | DELETE | http://localhost:8090/api/sales/<int:id>

To create sales
```
 {
    "automobile": "2HKRM4H35GH688819",
    "salesperson": 1,
    "customer": 2,
    "price": 1000
}

from django.db import models
from django.urls import reverse

# Create your models here.


class AutomobileVO (models.Model):
    vin = models.CharField(max_length=100)
    sold = models.BooleanField()
    import_href = models.CharField(max_length=200, unique=True)


class Salesperson(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.PositiveSmallIntegerField(unique=True)

    def get_api_url(self):
        return reverse("api_show_salesperson", kwargs={"pk": self.pk})


class Customer(models.Model):
    first_name = models.CharField(max_length=200, unique=True)
    last_name = models.CharField(max_length=200, unique=True)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=15, unique=True)

    def get_api_url(self):
        return reverse("api_show_customer", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name


class Sale(models.Model):
    price = models.PositiveIntegerField()
    customer = models.ForeignKey(Customer, related_name="customer", on_delete=models.SET_NULL, null=True, blank=False)
    automobile = models.ForeignKey(AutomobileVO, related_name="automobile", on_delete=models.SET_NULL, null=True, blank=False)
    Salesperson = models.ForeignKey(Salesperson, related_name= "salesperson", on_delete=models.SET_NULL, null=True, blank=False)

    def get_api_url(self):
        return reverse("api_show_customer", kwargs={"pk": self.pk})

import { Link } from 'react-router-dom';
import { useEffect, useState } from 'react';


function TechnicianList() {
    const [technicians , setTechnicians] = useState('');


    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setTechnicians(data.technicians)
        }
      }

    useEffect(() => {
        fetchData();
    }, []);

    const handleDelete = async (event) => {
      const data = {};
      data.id = event.technician.id;
      const url = 'http://localhost:8080/api/technicians/' + event.technician.id;
      const fetchConfig = {
        method: "delete",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        refreshPage()
      }
    }

    const refreshPage = () => {
      window.location.reload();
    }

  return (
    <div className="container">
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-end m-4">
              <Link to="/technicians/new" className="btn btn-primary btn-lg px-4 gap-3">Add a Technicians!</Link>
        </div>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Employee_id</th>
                <th>First_name</th>
                <th>Last_name</th>
                <th>Operate</th>
            </tr>
            </thead>
            <tbody>
            {Array.from(technicians).map((technician) => {
                return (
                <tr key={technician.id}>
                    <td>{ technician.employee_id }</td>
                    <td>{ technician.first_name }</td>
                    <td>{ technician.last_name }</td>
                    <td><button onClick={handleDelete.bind(this, {technician})}>Delete</button></td>
                </tr>
                );
            })}
            </tbody>
        </table>
      </div>
  );
}

export default TechnicianList;

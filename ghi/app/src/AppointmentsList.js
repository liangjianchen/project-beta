import { Link } from 'react-router-dom';
import { useEffect, useState } from 'react';
import moment from 'moment'


function AppointmentsList() {
    const [appointments , setAppointments] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/appointments/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setAppointments(data.appointments)
        }
      }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
      const data = {};

      const locationUrl = 'http://localhost:8080/api/appointments/'+ event.appointment.id+event.operate;
      if(event.operate === 'cancel'){
        data.status = true
        const fetchConfig = {
          method: "delete",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          refreshPage();
        }
      }else{
        // operate is finish
        data.status = true
        const fetchConfig = {
          method: "put",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          refreshPage();
        }
      }
  }

    const refreshPage = () => {
      window.location.reload();
    }

  return (
    <div className="container">
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-end m-4">
              <Link to="/appointments/new" className="btn btn-primary btn-lg px-4 gap-3">Add a Appointment!</Link>
        </div>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>VIN</th>
                <th>Is VIP</th>
                <th>customer name</th>
                <th>date</th>
                <th>time</th>
                <th>technician's name</th>
                <th>reason</th>
                <th>Operate</th>
            </tr>
            </thead>
            <tbody>
            {Array.from(appointments).map((appointment) => {
                return (
                <tr key={appointment.href} style={{display: appointment.status ? 'none':' '}}>
                    <td>{ appointment.vin }</td>
                    <td>{ appointment.vip ? 'Yes':'No' }</td>
                    <td>{ appointment.customer }</td>
                    {/* install moment and using moment to format data */}
                    <td>{ moment(appointments.date_time).format("YYYY-MM-DD") }</td>
                    <td>{ moment(appointments.date_time).format("HH:mm") }</td>
                    <td>{ appointment.technician.first_name +" " +appointment.technician.last_name}</td>
                    <td>{ appointment.reason}</td>
                    <td>
                        <button className='btn btn-outline-warning' onClick={handleSubmit.bind(this, {appointment,'operate':'/cancel'})}>Cancel</button>
                        <button className='btn btn-outline-warning' onClick={handleSubmit.bind(this, {appointment,'operate':'/finish'})}>Finish</button>
                    </td>
                </tr>
                );
            })}
            </tbody>
        </table>
      </div>
  );
}

export default AppointmentsList;

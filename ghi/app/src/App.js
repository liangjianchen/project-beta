import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import ModelsList from './ModelsList';
import ModelsForm from './ModelsForm';
import TechnicianList from './TechnicianList';
import TechnicianListForm from './TechnicianListForm';
import AppointmentsList from './AppointmentsList';
import AppointmentsForm from './AppointmentsForm';
import ServiceHistory from './ServiceHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers" >
            <Route path="" element={<ManufacturerList/>} />
            <Route path="new" element={<ManufacturerForm/>} />
          </Route>
          <Route path="models">
            <Route path="" element={<ModelsList/>} />
            <Route path="new" element={<ModelsForm/>} />
          </Route>
          <Route path="automobile">
            <Route path="" element={<AutomobileList/>} />
            <Route path="new" element={<AutomobileForm/>} />
          </Route>
          <Route path="technicians">
            <Route path="" element={<TechnicianList/>} />
            <Route path="new" element={<TechnicianListForm/>} />
          </Route>
          <Route path="appointments">
            <Route path="" element={<AppointmentsList/>} />
            <Route path="new" element={<AppointmentsForm/>} />
          </Route>
          <Route path="history">
            <Route path="" element={<ServiceHistory/>} />
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );

}

export default App;

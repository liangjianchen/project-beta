import { Link } from 'react-router-dom';
import React, { useEffect, useState } from 'react';


function ModelsList() {
    const [models, setModels] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setModels(data.models)
        }
      }

    useEffect(() => {
        fetchData();
    }, []);


  return (
    <div className="container">
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-end m-4">
              <Link to="/models/new" className="btn btn-primary btn-lg px-4 gap-3">Add a Model!</Link>
        </div>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Manufacturer</th>
                <th>picture</th>
            </tr>
            </thead>
            <tbody>
            {Array.from(models).map((model) => {
                return (
                <tr key={model.id}>
                    <td>{ model.name }</td>
                    <td>{ model.manufacturer.name }</td>
                    <td><img src={model.picture_url} className="img-fluid img-thumbnail" height={200} width={200}></img></td>
                </tr>
                );
            })}
            </tbody>
        </table>
      </div>
  );
}

export default ModelsList;

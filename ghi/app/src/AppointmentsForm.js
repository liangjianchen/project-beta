import {useState,useEffect} from 'react';

function AppointmentsForm(){
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [date_time, setDate_time] = useState('');
    const [technicians, setTechnicians] = useState('');
    const [reason, setReason] = useState('');
    const [technician_select, setTechnician_select] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.vin = vin;
        data.customer = customer;
        data.date_time = date_time;
        data.technician = technician_select;
        data.reason = reason;
        const locationUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          setVin('');
          setCustomer('');
          setDate_time('');
          setTechnician_select('');
          setReason('');
          refreshPage();
        }
    }

    const refreshPage = () => {
      window.location.reload();
    }


    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handleDateChange = (event) => {
      const value = event.target.value;
      setDate_time(value);
    }

    const handleVinChange = (event) => {
      const value = event.target.value;
      setVin(value);
    }

    const handleTechnicianChange = (event) => {
      const value = event.target.value;
      setTechnician_select(value);
    }

    const handleFReasonChange = (event) => {
      const value = event.target.value;
      setReason(value);
    }


    const fetchData = async () => {
      const url = 'http://localhost:8080/api/technicians/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setTechnicians(data.technicians)
      }
    }

    useEffect(() => {
      fetchData();
  }, []);




    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Service Appointment!</h1>
            <form onSubmit={handleSubmit}  id="create-manufacturer-form">
              <div className="form-floating mb-3">
                <input onChange={handleVinChange} value={vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control"/>
                <label htmlFor="employee_id">Automobile VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCustomerChange} value={customer} placeholder="customer" required type="text" name="customer" id="customer" className="form-control"/>
                <label htmlFor="customer">Customer name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleDateChange} value={date_time} placeholder="date_time" required type="datetime-local" name="date_time" id="date_time" className="form-control"/>
                <label htmlFor="date_time">Appointment Date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFReasonChange} value={reason} placeholder="reason" required type="text" name="reason" id="reason" className="form-control"/>
                <label htmlFor="reason">Reason</label>
              </div>
              <div className="form-floating mb-3">
                <select required onChange={handleTechnicianChange} value={technician_select} name="technician_select" id="technician_select" className="form-select">
                    <option value="">Choose a technician</option>
                      {Array.from(technicians).map((technician) => {
                      return (
                        <option key={technician.id} value={technician.id}>{technician.first_name+ "  " +technician.last_name }</option>
                      )
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Add</button>
            </form>

          </div>
        </div>
      </div>
      );
}
export default AppointmentsForm;

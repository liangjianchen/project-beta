import { Link } from 'react-router-dom';
import React, { useEffect, useState } from 'react';


function ManufacturerList() {
    const [manufacturers, setManufacturers] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setManufacturers(data.manufacturers)
        }
      }

    useEffect(() => {
        fetchData();
    }, []);


  return (
    <div className="container">
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-end m-4">
              <Link to="/manufacturers/new" className="btn btn-primary btn-lg px-4 gap-3">Add a Manufacturer!</Link>
        </div>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
            </tr>
            </thead>
            <tbody>
            {Array.from(manufacturers).map((manufacturer) => {
                return (
                <tr key={manufacturer.id}>
                    <td>{ manufacturer.name }</td>
                </tr>
                );
            })}
            </tbody>
        </table>
      </div>
  );
}

export default ManufacturerList;

import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          </ul>
        </div>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Manufacturers
              </a>
                <ul className="dropdown-menu">
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/manufacturers">Manufacturers List</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/manufacturers/new">Create Manufacturers</NavLink>
                  </li>
                </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Models
              </a>
                <ul className="dropdown-menu">
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/models">Models List</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/models/new">Create Models</NavLink>
                  </li>
                </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Automobile
              </a>
                <ul className="dropdown-menu">
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/automobile">Automobile List</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/automobile/new">Create Automobile</NavLink>
                  </li>
                </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Technician
              </a>
                <ul className="dropdown-menu">
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/technicians">Technician List</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/technicians/new">Create Technician</NavLink>
                  </li>
                </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Appointments
              </a>
                <ul className="dropdown-menu">
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/appointments">Appointments List</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/appointments/new">Create Appointment</NavLink>
                  </li>
                </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Service History
              </a>
                <ul className="dropdown-menu">
                  <li className="nav-item">
                    <NavLink className="dropdown-item" to="/history">Service History</NavLink>
                  </li>
                </ul>
            </li>
          </ul>
        </div>

      </div>
    </nav>
  )
}

export default Nav;

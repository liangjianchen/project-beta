import { useEffect, useState } from 'react';
import moment from 'moment'


function ServiceHistory() {
    const [appointments , setAppointments] = useState('');
    const [vin, setVin] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/appointments/';
        const response = await fetch(url);
        if (response.ok) {
          const data = await response.json();
          setAppointments(data.appointments)
        }
      }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
      event.preventDefault();
      const url = 'http://localhost:8080/api/search/'+vin
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setAppointments(data.appointments)
      }
  }

  const refreshPage = () => {
      window.location.reload();
    }

  const handleVinChange = (event) => {
      const value = event.target.value;
      setVin(value);
    }

  return (
    <div className="container">
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-end m-4">
            <form className="d-flex" role="search" onSubmit={handleSubmit}>
              <input className="form-control me-2" aria-label="Search" onChange={handleVinChange} value={vin} placeholder="vin" required type="text" name="vin" id="vin" />
              <button className="btn btn-outline-success" type="submit">Search</button>
            </form>
        </div>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>VIN</th>
                <th>Is VIP</th>
                <th>customer name</th>
                <th>date</th>
                <th>time</th>
                <th>technician's name</th>
                <th>reason</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            {Array.from(appointments).map((appointment) => {
                return (
                <tr key={appointment.href}>
                    <td>{ appointment.vin }</td>
                    <td>{ appointment.vip ? 'Yes':'No' }</td>
                    <td>{ appointment.customer }</td>
                    {/* install moment and using moment to format data */}
                    <td>{ moment(appointments.date_time).format("YYYY-MM-DD") }</td>
                    <td>{ moment(appointments.date_time).format("HH:mm") }</td>
                    <td>{ appointment.technician.first_name +" " +appointment.technician.last_name}</td>
                    <td>{ appointment.reason}</td>
                    <td>{ appointment.operate}</td>
                </tr>
                );
            })}
            </tbody>
        </table>
      </div>
  );
}

export default ServiceHistory;

import {useState} from 'react';

function TechnicianListForm(){
    const [employee_id, setEmployee_id] = useState('');
    const [first_name, setFirst_name] = useState('');
    const [last_name, setLast_name] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.employee_id = employee_id;
        data.first_name = first_name;
        data.last_name = last_name;
        const locationUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          setEmployee_id('');
          setFirst_name('');
          setLast_name('');
        }
    }

    const handleFNameChange = (event) => {
        const value = event.target.value;
        setFirst_name(value);
    }

    const handleLNameChange = (event) => {
      const value = event.target.value;
      setLast_name(value);
    }

    const handleIdChange = (event) => {
      const value = event.target.value;
      setEmployee_id(value);
    }



    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new Technicians!</h1>
            <form onSubmit={handleSubmit}  id="create-manufacturer-form">
              <div className="form-floating mb-3">
                <input onChange={handleIdChange} value={employee_id} placeholder="employee_id" required type="number" name="employee_id" id="employee_id" className="form-control"/>
                <label htmlFor="employee_id">Employee_id</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFNameChange} value={first_name} placeholder="first_name" required type="text" name="first_name" id="first_name" className="form-control"/>
                <label htmlFor="first_name">First_name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleLNameChange} value={last_name} placeholder="last_name" required type="text" name="last_name" id="last_name" className="form-control"/>
                <label htmlFor="last_name">Last_name</label>
              </div>
              <button className="btn btn-primary">Add</button>
            </form>

          </div>
        </div>
      </div>
      );
}
export default TechnicianListForm;

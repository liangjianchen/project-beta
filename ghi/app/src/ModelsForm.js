import {useState, useEffect} from 'react';

function ModelsForm(){
    const [name, setName] = useState('');
    const [picture_url, setPicture_url] = useState('');
    const [manufacturer_id, setManufacturer_id] = useState('');
    const [manufacturers, setManufacturers] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.picture_url = picture_url;
        data.manufacturer_id = manufacturer_id;

        const locationUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          setName('');
          setPicture_url('');
          setManufacturer_id('');
        }
    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handlePicture_urlChange = (event) => {
      const value = event.target.value;
      setPicture_url(value);
    }

    const handleManufacturer_idChange = (event) => {
      const value = event.target.value;
      setManufacturer_id(value);
    }

    const fetchData = async () => {
      const url = 'http://localhost:8100/api/manufacturers/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setManufacturers(data.manufacturers);
      }
    }

    useEffect(() => {
      fetchData();
    }, []);



    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new Model!</h1>
            <form onSubmit={handleSubmit}  id="create-manufacturer-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePicture_urlChange} value={picture_url} placeholder="Picture_url" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                <label htmlFor="picture_url">Picture_url</label>
              </div>
              <div className="mb-3">
                <select required onChange={handleManufacturer_idChange} value={manufacturer_id}  name="manufacturer_id" id="manufacturer_id" className="form-select">
                    <option value="">Choose a manufacturer</option>
                    {Array.from(manufacturers).map((manufacturer) => {
                      return (
                        <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                      )
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Add</button>
            </form>

          </div>
        </div>
      </div>
      );
}
export default ModelsForm;

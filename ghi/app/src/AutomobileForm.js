import {useState,useEffect} from 'react';

function AutomobileForm(){
    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');
    const [model_id, setModel_id] = useState('');
    const [models, setModels] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.color = color;
        data.year = parseInt(year) ;
        data.vin = vin;
        data.model_id = parseInt(model_id);
        const locationUrl = '	http://localhost:8100/api/automobiles/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          setColor('');
          setYear('');
          setVin('');
          setModel_id('');
        }
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleYearChange = (event) => {
      const value = event.target.value;
      setYear(value);
    }

    const handleVinChange = (event) => {
      const value = event.target.value;
      setVin(value);
    }

    const handleModelIdChange = (event) => {
      const value = event.target.value;
      setModel_id(value);
    }

    const fetchData = async () => {
      const url = 'http://localhost:8100/api/models/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setModels(data.models);
      }
    }

    useEffect(() => {
      fetchData();
    }, []);



    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new Automobile!</h1>
            <form onSubmit={handleSubmit}  id="create-manufacturer-form">
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleYearChange} value={year} placeholder="Year" required type="number" name="year" id="year" className="form-control"/>
                <label htmlFor="year">Year</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleVinChange} value={vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control"/>
                <label htmlFor="vin">Vin</label>
              </div>
              <div className="mb-3">
                <select required onChange={handleModelIdChange} value={model_id}  name="model_id" id="model_id" className="form-select">
                    <option value="">Choose a Model</option>
                    {Array.from(models).map((model) => {
                      return (
                        <option key={model.id} value={model.id}>{model.name}</option>
                      )
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Add</button>
            </form>

          </div>
        </div>
      </div>
      );
}
export default AutomobileForm;

from django.db import models
from django.urls import reverse

# Create your models here.


class Technician(models.Model):
    employee_id = models.IntegerField()
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.first_name}"

    def get_api_url(self):
        return reverse("api_delete_technicians", kwargs={"id": self.id})


class AutomobileVO (models.Model):
    vin = models.CharField(max_length=100)
    sold = models.BooleanField()
    import_href = models.CharField(max_length=200, unique=True)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    status = models.BooleanField(default=False)
    reason = models.CharField(max_length=150)
    vin = models.CharField(max_length=100)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE,
    )
    vip = models.BooleanField(default=False)
    operate = models.CharField(max_length=50, default="Create")

    def get_api_url(self):
        return reverse("api_deleteOrUpdate_appointments", kwargs={"id": self.id})

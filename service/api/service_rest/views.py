from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json
from django.http import JsonResponse
from .models import AutomobileVO, Technician, Appointment
# Create your views here.


class AutomobileVODetailEnccoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold", "employee_id", "import_href"]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name", "last_name", "employee_id", "id"]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin", "date_time", "status", "reason", "customer", "technician", "vip","id", "operate"
        ]
    encoders = {
        "technician": TechnicianListEncoder()
    }



@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianListEncoder,
                safe=False,
            )
        except Exception as e:
            return JsonResponse(
                {"message": e},
            )


@require_http_methods(["DELETE"])
def api_delete_technicians(request, id=None):
    if request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=id)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianListEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
        )
    else:
        try:
            # get the isinstance from the Technician by id
            content = json.loads(request.body)
            technician_id = content['technician']
            technician_instance = Technician.objects.get(id=technician_id)
            content['technician'] = technician_instance
            appointment = Appointment.objects.create(**content)
            # get the AutomobileVO data by using vin
            automobile = AutomobileVO.objects.get(vin = content['vin'])
            if automobile is not None:
                appointment.vip = True
                appointment.save()
            return JsonResponse(
                    appointment,
                    encoder=AppointmentListEncoder,
                    safe=False,
                )
        except Exception as e:
            return JsonResponse(
                {"message": e},
                status=400,
            )


@require_http_methods(["DELETE", "PUT"])
def api_deleteOrUpdate_appointments(request, id=None):
    if request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.status = True
            appointment.operate = 'Delete'
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.status = True
            appointment.operate = 'Finish'
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET"])
def api_search_appointments(request, vin):
    if request.method == "GET":
        try:
            appointments = Appointment.objects.filter(vin = vin)
            return JsonResponse(
                {"appointments": appointments},
                encoder=AppointmentListEncoder,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})

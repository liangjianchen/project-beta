from django.urls import path
from .views import api_list_technicians, api_delete_technicians, api_list_appointments, api_deleteOrUpdate_appointments,api_search_appointments

urlpatterns = [
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("technicians/<int:id>", api_delete_technicians, name="api_delete_technicians"),
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path("appointments/<int:id>/cancel", api_deleteOrUpdate_appointments, name="api_deleteOrUpdate_appointments"),
    path("appointments/<int:id>/finish", api_deleteOrUpdate_appointments, name="api_deleteOrUpdate_appointments"),
    path("search/<vin>", api_search_appointments, name="api_search_appointments"),
]
